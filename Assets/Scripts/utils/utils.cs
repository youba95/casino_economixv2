﻿using System;
using System.Text;

namespace Scripts.model
{
    public class utils
    {
        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmss");
        }

    }
}
