﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.service;
using Scripts.model;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.WSA;
using Valve.VR.InteractionSystem.Sample.config;
using Random = UnityEngine.Random;

namespace Valve.VR.InteractionSystem.Sample
{
	public class MachineController : MonoBehaviour
	{
		public HoverButton ButtonLessMoney;
		public HoverButton ButtonMoreMoney;
		public HoverButton ButtonGameA;
		public HoverButton ButtonGameB;
		public HoverButton ButtonStart;
		public HoverButton ButtonPower;
		public ICsvHandler csvP;
		public ICsvHandler csvA;


		// Player starts with 9 coins and a bet of 0 and has to bet at least 0
		private int Coins;
		private int bet;
		private int minimumBet;

		// 1 = gameA, 2 = gameB
		public int gameMode; 
		public int gameModeAvatar; 


		// isOn = the machine is powered on
		public bool isOn = true;

		// we need to retrieve the screen GameObject to power it on and off
		public GameObject screen;

		// All the UI texts
		public Text TextApplicationName;
		public Text TextApplicationInfo;
		public Text TextCurrentCoins;
		public Text TextCurrentBet;
		public Text TextCurrentGameMode;
		public Text resultMsg;
		public GameObject woman;
		public GameObject man;
		
		

		// We retrieve Panel gameobjects in order to change the panels color regarding to the gameMode
		public GameObject PanelApplicationName;
		public GameObject PanelApplicationInfo;
		public GameObject PanelCurrentCoins;
		public GameObject PanelCurrentBet;
		public GameObject PanelCurrentGameMode;

		public bool canSwitch = true;
		public bool waitActive = false;
		// Game over
		private bool gameOver = false;
		public bool enCours = false;
		public int scenario; //initialisation du scénario à 1
		private void Start() {
			man = GameObject.Find("PlayerPNJ4_M");
			woman = GameObject.Find("PNJWomanTalking1");
			if (GameConfig.avatar == "H")
			{
				Destroy(woman);
			}
			else
			{
				Destroy(man);
			}
			ButtonLessMoney.onButtonDown.AddListener(removeBet);
			ButtonMoreMoney.onButtonDown.AddListener(addBet);
			ButtonGameA.onButtonDown.AddListener(switchToA);
			ButtonGameB.onButtonDown.AddListener(switchToB);
			ButtonStart.onButtonDown.AddListener(startGame);
			ButtonPower.onButtonDown.AddListener(powerMachine);
			Coins = GameConfig.coinsPlayer;
			bet = GameConfig.bet;
			gameMode = GameConfig.gameMode;
			gameModeAvatar = GameConfig.gameModeAvatar;
			resultMsg.enabled = false;
			csvP = csvPlayer.Instance();
			csvA = csvAvatar.Instance();
			
			scenario = (GameConfig.scenarioAvatar == "G") ? 1 : 2;
		}
		public void setScenario(int i)
		{
			this.scenario = i;
		}
		void Update () {

			screen.SetActive (isOn);

			if (isOn) {
				adjustBet ();
				if (this.name == "CasinoMachineAvatar")
				{
					updateUIAvatar();
				}
				else updateUI ();
			}
		}
		
		
		public void removeBet (Hand hand) {
			if(bet > 0 && !gameOver)
				bet -= 1;
		}

		public void addBet (Hand hand) {
			if (bet < Coins && !gameOver)
				bet += 1;
		}

		public void switchToA(Hand hand) {
			if (gameOver)
				return;
			gameMode = 1;
		}

		public void switchToB(Hand hand) {
			if (gameOver)
				return;
			gameMode = 2;
		}

		public void startGame(Hand hand)
		{
			enCours = true;
			if (!isOn || gameOver)
				return;

			if (bet < minimumBet) {
				TextApplicationInfo.text = "Parier plus !";
				return;
			}

			//TextApplicationInfo.text = "Drawing...";
			Coins -= bet;

			int result = -1;

			if (gameMode == 1) 
				result = Random.Range (0, 2);

			if (gameMode == 2) 
				result = Random.Range (0, 6);


			if (result == 0) {
				if (gameMode == 1) {
					TextApplicationInfo.text = "Gagné ! +" + bet + "*3 jetons !";
					Coins += bet * 3;
				}
				if (gameMode == 2) {
					TextApplicationInfo.text = "Gagné ! +" + bet + "*9 jetons !";
					Coins += bet * 9;
				}
				resultMsg.text = "GAGNÉS !!";
				flankjankWon();
				StartCoroutine(routine(resultMsg));

			} else {
				TextApplicationInfo.text = "Perdu... -" + bet + " jetons !";
				if (Coins == 0) {
					gameOver = true;
					TextCurrentCoins.text = "$$$ : " + Coins;
				}
				resultMsg.text = "PERDU !!";
				flankjankLose();
				StartCoroutine(routine(resultMsg));
			}
			var g = new gamePlayer(
				utils.GetTimestamp(DateTime.Now),
				(gameMode == 1 ) ? "A" : "B",
				bet, 
				(result == 0) ? "Win" : "Lose",
				Coins
			);
			csvP.appendLine(g);
			csvP.save();
			Debug.Log (result);
			enCours = false;
		}

		public void powerMachine(Hand hand) {
			isOn = !isOn;
			if (!isOn)
				resetMachine ();
		}
		public void removeBet () {
			if(bet > 0 && !gameOver)
				bet -= 1;
		}

		public void addBet () {
			if (bet < Coins && !gameOver)
				bet += 1;
		}

		public void switchToA() {
			if (gameOver)
				return;
			gameMode = 1;
		}

		public void switchToB() {
			if (gameOver)
				return;
			gameMode = 2;
		}

		public int startGameForAvatar() {
			if (!isOn || gameOver)
				return -1;
			
			if (bet < minimumBet) {
				TextApplicationInfo.text = "Parier plus !";
				return -1;
			}

			Coins -= bet;

			int result = -1;

			if (scenario == 1)
			{
				result = Random.Range(0, 2);
			}
			else if (scenario == 2) 
			{
				result = Random.Range(0, 8);
			}
			else
			{
				if (gameModeAvatar == 1)
					result = Random.Range(0, 2);

				if (gameModeAvatar == 2)
					result = Random.Range(0, 6);
			}
			
			if (result == 0) {
				if (gameModeAvatar == 1) {
					TextApplicationInfo.text = "Gagné ! +" + bet + "*3 jetons !";
					Coins += bet * 3;
				}
				if (gameModeAvatar == 2) {
					TextApplicationInfo.text = "Gagné ! +" + bet + "*9 jetons !";
					Coins += bet * 9;
				}
				var avatarPlayer = new avatarPlayer(
					utils.GetTimestamp(DateTime.Now),
					"Win"
				);
				csvA.appendLine(avatarPlayer);
				csvA.save();
				return 1;

			} else {
				TextApplicationInfo.text = "Perdu... -" + bet + " jetons !";
				if (Coins == 0) {
					gameOver = true;
					TextCurrentCoins.text = "$$$ : " + Coins;
				}
				var avatarPlayer = new avatarPlayer(
					utils.GetTimestamp(DateTime.Now),
					"Lose"
				);
				csvA.appendLine(avatarPlayer);
				csvA.save();
				return 0;
			}
		}

		public void powerMachine() {
			isOn = !isOn;
			if (!isOn)
				resetMachine ();
		}

		public void resetMachine() {
			Coins = 20;
			bet = 1;
			gameMode = 1;
			gameOver = false;
			TextApplicationInfo.text = "Placer des paris !";
		}

		public void updateUI() {
			
			TextApplicationName.text = "Economix";

			if (gameOver) {
				TextApplicationInfo.text = "Game over...";
				return;
			}

			TextCurrentCoins.text = "Credit : " + Coins;
			TextCurrentBet.text = "Pari : " + bet;
			
			
			if (gameMode == 1 && canSwitch) {
				TextCurrentGameMode.text = "Mode de jeu A : 1 chance sur 2 \n de gagner 3 fois la mise !";

				PanelApplicationName.GetComponent<Image>().color = new Color(0, 143, 228);
				PanelApplicationInfo.GetComponent<Image>().color = new Color(0, 143, 228);
				PanelCurrentCoins.GetComponent<Image>().color = new Color(0, 143, 228);
				PanelCurrentBet.GetComponent<Image>().color = new Color(0, 143, 228);
				PanelCurrentGameMode.GetComponent<Image>().color = new Color(0, 143, 228);
			}
			if (gameMode == 2 && canSwitch) {
				TextCurrentGameMode.text = "Mode de jeu B : 1 chance sur 6 \n de gagner 9 fois la mise !";

				PanelApplicationName.GetComponent<Image>().color = new Color(255, 234, 0);
				PanelApplicationInfo.GetComponent<Image>().color = new Color(255, 234, 0);
				PanelCurrentCoins.GetComponent<Image>().color = new Color(255, 234, 0);
				PanelCurrentBet.GetComponent<Image>().color = new Color(255, 234, 0);
				PanelCurrentGameMode.GetComponent<Image>().color = new Color(255, 234, 0);
			}
		}
		
		public void updateUIAvatar() {
			
			TextApplicationName.text = "Economix";

			if (gameOver) {
				TextApplicationInfo.text = "Game over...";
				return;
			}

			TextCurrentCoins.text = "Credit : " + Coins;
			TextCurrentBet.text = "Pari : " + bet;
			
			
			if (gameModeAvatar == 1 && canSwitch) {
				TextCurrentGameMode.text = "Mode de jeu A : 1 chance sur 2 \n de gagner 3 fois la mise !";

				PanelApplicationName.GetComponent<Image>().color = new Color(0, 143, 228);
				PanelApplicationInfo.GetComponent<Image>().color = new Color(0, 143, 228);
				PanelCurrentCoins.GetComponent<Image>().color = new Color(0, 143, 228);
				PanelCurrentBet.GetComponent<Image>().color = new Color(0, 143, 228);
				PanelCurrentGameMode.GetComponent<Image>().color = new Color(0, 143, 228);
			}
			if (gameModeAvatar == 2 && canSwitch) {
				TextCurrentGameMode.text = "Mode de jeu B : 1 chance sur 6 \n de gagner 9 fois la mise !";

				PanelApplicationName.GetComponent<Image>().color = new Color(255, 234, 0);
				PanelApplicationInfo.GetComponent<Image>().color = new Color(255, 234, 0);
				PanelCurrentCoins.GetComponent<Image>().color = new Color(255, 234, 0);
				PanelCurrentBet.GetComponent<Image>().color = new Color(255, 234, 0);
				PanelCurrentGameMode.GetComponent<Image>().color = new Color(255, 234, 0);
			}
		}

		// If bet is higher than current coins, set bet to current coins to avoid having a negative amount of coins in case we lose
		public void adjustBet() {
			if (bet > Coins)
				bet = Coins;
		}
		
		public IEnumerator routine(Text text)
		{   
			text.enabled = true;
			yield return new WaitForSeconds(3);
			text.enabled = false;

		}
		IEnumerator Wait(){
			waitActive = true;
			yield return new WaitForSeconds (3.0f);
			canSwitch = true;
			waitActive = false;
		}
		
		public void flankjankWon()
		{
			PanelApplicationName.GetComponent<Image>().color = Color.green;
			PanelApplicationInfo.GetComponent<Image>().color = Color.green;
			PanelCurrentCoins.GetComponent<Image>().color = Color.green;
			PanelCurrentBet.GetComponent<Image>().color = Color.green;
			PanelCurrentGameMode.GetComponent<Image>().color = Color.green;
			if(!waitActive){
				StartCoroutine(Wait());   
			}
			if(canSwitch){
				TextApplicationInfo.enabled = true;
				TextApplicationName.enabled = true;
				TextCurrentBet.enabled = true;
				TextCurrentCoins.enabled = true;
				TextCurrentGameMode.enabled = true;
				canSwitch = false;
			}
		}

		public void flankjankLose()
		{
	
			PanelApplicationName.GetComponent<Image>().color = Color.red;
			PanelApplicationInfo.GetComponent<Image>().color = Color.red;
			PanelCurrentCoins.GetComponent<Image>().color = Color.red;
			PanelCurrentBet.GetComponent<Image>().color = Color.red;
			PanelCurrentGameMode.GetComponent<Image>().color = Color.red;
			if(!waitActive){
				StartCoroutine(Wait());   
			}
			if(canSwitch){
				TextApplicationInfo.enabled = true;
				TextApplicationName.enabled = true;
				TextCurrentBet.enabled = true;
				TextCurrentCoins.enabled = true;
				TextCurrentGameMode.enabled = true;
				canSwitch = false;
			}
			

		}


	}
}
