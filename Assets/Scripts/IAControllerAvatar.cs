﻿	﻿using System;
	 using System.Collections;
using System.Collections.Generic;
	 using System.Runtime.CompilerServices;
	 using UnityEngine;
using Valve.VR.InteractionSystem.Sample;
	 using Valve.VR.InteractionSystem.Sample.config;

    public class IAControllerAvatar : MonoBehaviour {

	private GameObject go;
	private GameObject machineAvatar;
	private MachineController ma;
	private MachineController maavatar;
	private MachineController m;
	public bool canSwitch = false;
	public bool waitActive = false;

	private enum AvatarState {Idle, Pushing, Result};
	public int animationTransitionRate= 10; // the higher the slower
	public int gameMode = 1; //1=A, 2=B

	private int messyCounter;
	private Animator animator;
	private AvatarState state;
	private int nb;
	private int res;
	

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
		state = AvatarState.Idle;
		messyCounter = animationTransitionRate;
        go = GameObject.Find("CasinoMachinePlayer");
        machineAvatar = GameObject.Find("CasinoMachineAvatar");
        m = machineAvatar.GetComponent<MachineController>();
		ma = go.GetComponent<MachineController>();
		m.gameMode = gameMode;
		m.resetMachine();
		m.powerMachine();
	}

	IEnumerator waitForAnimation( float sec, int res)
	{
		
		yield return new WaitForSeconds(sec+1.0f);
		if (res == 1)
		{
			m.flankjankWon();
			m.resultMsg.text = "GAGNÉ !!";
			StartCoroutine(m.routine(m.resultMsg));
		}
		else
		{
			m.flankjankLose();
			m.resultMsg.text = "PERDU !!";
			StartCoroutine(m.routine(m.resultMsg));
		}
		
		//animator.Play(animation, -1, 0f);
		Debug.Log("Finished Coroutine at timestamp : " + Time.time);
   
	}
	void scenario()
	{
		res = m.startGameForAvatar();
		if (res == 1)
		{
			animator.Play("Button_Pushing 0", -1, 0f);
			StartCoroutine(waitForAnimation(animator.GetCurrentAnimatorStateInfo(0).length, res));
		}
		else
		{
			animator.Play("Button_Pushing", -1, 0f);
			StartCoroutine(waitForAnimation(animator.GetCurrentAnimatorStateInfo(0).length, res));
		}

		this.nb++;
		
	}

	void Update()
	{

		if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Defeat") && 
		    !animator.GetCurrentAnimatorStateInfo(0).IsName("Victory Idle") &&
		    nb < GameConfig.nbtoursMaxAvatar){
			if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Button_Pushing 0") &&
			    !animator.GetCurrentAnimatorStateInfo(0).IsName("Button_Pushing"))
			{
				scenario();
			}
			
		}
			
		
		if (Input.GetKeyDown("g"))
		{
			animator.Play("Victory Idle", -1, 0f);
			Debug.Log("test");
		}
		if (Input.GetKeyDown(KeyCode.Return))
		{
			Debug.Log(ma.enCours);
			if(!ma.enCours)
				ma.startGame(null);
			
		}
		if (Input.GetKeyDown("d"))
		{
			m.startGame(null);
		}
		if (Input.GetKeyDown("v"))
		{
			ma.switchToA();
		}
		if (Input.GetKeyDown("b"))
		{
			ma.switchToB();
		}
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			ma.powerMachine();
		}
		if (Input.GetKeyDown("p"))
		{
			ma.addBet();
		}
		if (Input.GetKeyDown("m"))
		{
			ma.removeBet();
		}
		if (Input.GetKeyDown("t"))
		{
			m.setScenario(1);
		}
		if (Input.GetKeyDown("i"))
		{
			m.setScenario(3);
		}
		if (Input.GetKeyDown("y"))
		{
			m.setScenario(2);
		}

	}
}
