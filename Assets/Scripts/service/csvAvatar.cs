﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using Scripts.model;
using Valve.VR.InteractionSystem.Sample.config;

namespace Assets.Scripts.service
{
    public class csvAvatar : ICsvHandler
    {
        static csvAvatar _singleton = null;
        private StringBuilder sb;
        private string folder;
        private string fileName;

        protected csvAvatar()
        {
            sb = new StringBuilder();
            folder = GameConfig.pathCsv;
            fileName = GameConfig.pathCsv + System.IO.Path.DirectorySeparatorChar + 
                       "avatarStat_" + 
                       GameConfig.avatar + 
                       "_tours_" +
                       GameConfig.nbtoursMaxAvatar +
                       "_Scenario_" + GameConfig.scenarioAvatar +
                       ".csv";
            creatHeader();
        }
        public void creatHeader()
        {
            var playerType = typeof (avatarPlayer);
            string header = "";
            foreach (var property in playerType.GetProperties())
            {
                var propertyAttributes = property;
                header = header + propertyAttributes.Name + ";";
            }
            header = header.Remove(header.Length - 1);
            sb.AppendLine(header);
            this.save();
        }

        public void appendLine(object o)
        {
            var gameP = (avatarPlayer) o;
            Type type = typeof(avatarPlayer);
            PropertyInfo[] properties = type.GetProperties();
            string line = "";
            foreach (PropertyInfo property in properties)
            {
                var a = property.GetValue(gameP, null);
                line = line + a.ToString() + ";";
            }
            line = line.Remove(line.Length - 1);
            sb.AppendLine(line);
            this.save();
        }

        public static csvAvatar Instance()
        {
            if (_singleton == null)
            {
                _singleton = new csvAvatar();
            }

            return _singleton;
        }

        public void save()
        {
            File.AppendAllText(fileName, sb.ToString());
            this.sb.Length = 0;
        }
    }
}