﻿using Scripts.model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Valve.VR.InteractionSystem.Sample.config;

namespace Assets.Scripts.service
{
    public class csvPlayer : ICsvHandler
    {
        static csvPlayer _singleton = null;
        private StringBuilder sb;
        private string folder;
        private string fileName;

        protected csvPlayer()
        {
            sb = new StringBuilder();
            folder = GameConfig.pathCsv;
            fileName = GameConfig.pathCsv + System.IO.Path.DirectorySeparatorChar + "gameStat.csv";
            creatHeader();
        }

        public static csvPlayer Instance()
        {
            if (_singleton == null)
            {
                _singleton = new csvPlayer();
            }

            return _singleton;
        }

        public void appendLine(object o)
        {
            var gameP = (gamePlayer) o;
            Type type = typeof(gamePlayer);
            PropertyInfo[] properties = type.GetProperties();
            string line = "";
            foreach (PropertyInfo property in properties)
            {
                var a = property.GetValue(gameP, null);
                line = line + a.ToString() + ";";
            }
            line = line.Remove(line.Length - 1);
            sb.AppendLine(line);
            this.save();
        }
        
        public void creatHeader()
        {
            var playerType = typeof (gamePlayer);
            string header = "";
            foreach (var property in playerType.GetProperties())
            {
                var propertyAttributes = property;
                header = header + propertyAttributes.Name + ";";
            }
            header = header.Remove(header.Length - 1);
            sb.AppendLine(header);
            this.save();
        }

        public void save()
        {
            File.AppendAllText(fileName, sb.ToString());
            this.sb.Length = 0;
        }
    }
}
