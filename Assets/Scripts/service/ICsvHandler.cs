﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Scripts.model;

namespace Assets.Scripts.service
{
    public interface ICsvHandler
    {
        void creatHeader();
        void appendLine(object o);
        void save();
    }
}
