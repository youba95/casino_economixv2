﻿using System;
using System.Xml.Serialization;
using Scripts.model;
using UnityEngine;

namespace Scripts.model
{
    public class gamePlayer
    {
        public string timeStamp { get; set; }
        public string gameMode { get; set; }
        public int bet { get; set; }
        public string result { get; set; }
        public int credit { get; set; }

        public gamePlayer(string timeStamp, string gameMode, int bet, string result, int credit)
        {
            this.timeStamp = timeStamp;
            this.gameMode = gameMode;
            this.bet = bet;
            this.result = result;
            this.credit = credit;
        }
    }
}