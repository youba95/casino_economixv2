﻿namespace Scripts.model
{
    public class avatarPlayer
    {
        public string timeStamp { get; set; }
        public string result { get; set; }

        public avatarPlayer(string timeStamp, string result)
        {
            this.timeStamp = timeStamp;
            this.result = result;
        }
    }
}