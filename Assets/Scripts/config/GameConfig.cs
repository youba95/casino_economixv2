﻿using System;
using System.Collections.Generic;
using System.IO;
using Scripts.model;
using UnityEngine;
using Valve.Newtonsoft.Json.Linq;

namespace Valve.VR.InteractionSystem.Sample.config
{
    public class GameConfig
    {
        public static int coinsPlayer = 20;
        public static int bet = 0;
        public static int gameMode = 1;
        public static int nbtoursMax = 10;

        public static string scenarioAvatar = "G";
        public static int gameModeAvatar = 1;
        public static string avatar = "H";
        public static int nbtoursMaxAvatar = 10;

        public static string pathCsv;

        public static void initialize(JObject avatarsParams, JObject machinePlayer, string path)
        {
            GameConfig.coinsPlayer = (int) machinePlayer["credit"];
            bet = (int) machinePlayer["miseDepart"];
            gameMode = (int) machinePlayer["gameMode"];
            nbtoursMax = (int) machinePlayer["nombreMaxTours"];
                
            scenarioAvatar = (string) avatarsParams["scenario"];
            gameModeAvatar = (int) avatarsParams["gameMode"];
            avatar = (string) avatarsParams["sexe"];
            nbtoursMaxAvatar = (int) avatarsParams["nombreMaxTours"];
            if(Directory.Exists(path))
            {
                pathCsv = path + System.IO.Path.DirectorySeparatorChar + "Stat__" + utils.GetTimestamp(DateTime.Now);
                System.IO.Directory.CreateDirectory(pathCsv);
            }
            else
            {
                pathCsv = "Stat__" + utils.GetTimestamp(DateTime.Now);
                System.IO.Directory.CreateDirectory(pathCsv);
            }
        }
    }
}