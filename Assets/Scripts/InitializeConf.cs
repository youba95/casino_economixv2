﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Valve.Newtonsoft.Json;
using Valve.Newtonsoft.Json.Linq;
using Valve.VR.InteractionSystem.Sample.config;

public class InitializeConf : MonoBehaviour {
	
	private void Awake()
	{
		Debug.Log ("initialisaiton des paramètres");
		
		if (File.Exists("conf.json"))
		{
			Debug.Log ("Fichier de config trouvé");
			var conf = File.ReadAllText("conf.json");
			
			JObject jObj = (JObject) JsonConvert.DeserializeObject(conf);
			JObject machinePlayer = (JObject) jObj["machinePlayer"];
			JObject avatarParams = (JObject) jObj["avatarsParams"];
			string pathData = (string) jObj["urlDonnees"];
			
			Debug.Log ("Valorisation des paramètres de config");
			GameConfig.initialize(avatarParams, machinePlayer, pathData);
		}
		else
		{
			Debug.Log ("Fichier de config non trouvé");
		}
	}
	
}
