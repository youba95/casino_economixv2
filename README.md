# casino_Economix V2
## Contexte 
Le projet consiste à faire une simulation logicielle sur le comportement de 
joueurs dans un casino. Pour cela, nous allons avoir deux machine de paris avec 
deux type de paris possible (un risqué et un moins risqué). Un avatar jouera à 
l'aide d'un script sur l'une des machines et le vrai joueur jouera sur l'autre. 
Le but sera d'analyser le comportement du vrai joueur vis à vis de l'avatar et
de savoir si celui-ci peut influencer le comportement du vrai joueur en 
fonction de ses gains. Nous devrons sortir un fichier excel avec la récupération
de toutes ces données que nos clients (les chercheuses) pourront analyser par 
la suite pour en tirer des conclusions.

## Prérequis
>   - Unity version 2019.3.xxxx 
 >   - steamVR 
    > - IDE : Rider ou Visual Studio
  >  - git-lfs
    
## Etapes de lancement
>   - Cloner ce dépot : https://gitlab.com/youba95/casino_economixv2/
 >   - Ouvrire le dossier avec Unity/Unity hub
 >   - Steam VR devrait se lancer automatiquement
    
## Configuation
Pour permettre au jeu de charger le fichier config.json
> au lancement du jeu :
>    - Cliquer sur le sol
>    - Dans la fenetre inspector, Add component -> Script -> InitializeConf

![](conf.jpg)
